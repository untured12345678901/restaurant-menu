import { DbModels } from "@root/server/database/db"
import DishImageManager from "@root/server/utilitary/dishImageManager"
import type { Request, Response } from "express"
import type { UploadedFile } from "express-fileupload"

export default class DishImageApiHandler {
    static async addImages(req: Request, res: Response) {
        const files: UploadedFile[] | [] = (() => {
            if (req.files && Object.keys(req.files).length) {
                return Object.values(req.files) as UploadedFile[]
            }
            return []
        })()
        const dishId = Number(req.body.dishId)

        if (!files.length) {
            res.status(400).json("Нет файлов для загрузки!")
            return
        }

        const images = []

        for (const file of files) {
            const image = await DbModels.dishImage.create({
                name: file.name,
                dishId
            }, file)

            images.push(image)
        }

        res.status(200).json(images)
    }
    static async deleteImage(req: Request, res: Response) {
        const dishImageId = Number(req.query.id)
        const deleted = await DbModels.dishImage.delete(dishImageId)
        
        res.status(200).json(deleted)
    }
    static async getImageFile(req: Request, res: Response) {
        const dishImageId = Number(req.query.id)
        const dishImage = await DbModels.dishImage.get(dishImageId)

        if (dishImage === null) {
            return res.status(400).json("Нет файлов для загрузки!")
        }

        res.status(200).sendFile(DishImageManager.resolveDishImageFilePath(dishImage))
    }
    static async getDishImages(req: Request, res: Response) {
        const dishId = Number(req.query.id)
        const images = await DbModels.dishImage.getByDishId(dishId)

        res.status(200).json(images)
    }
}