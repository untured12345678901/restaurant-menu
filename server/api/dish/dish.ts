import type { Request, Response } from "express";
import type { DishCreationParams } from "./types";
import { DbModels } from "@root/server/database/db";

export default class DishApiHandler {
    static async create(req: Request, res: Response) {
        const { name, description, categoryId } = req.body as DishCreationParams
        const dish = await DbModels.dish.create({name, description, categoryId})

        res.status(200).json(dish)
    }
    static async get(req: Request, res: Response) {
        const id = Number(req.query.id)

        const dish = await DbModels.dish.get(id)
        res.status(200).json(dish)
    }
    static async getWithImagesByCategory(req: Request, res: Response) {
        const categoryId = Number(req.query.categoryId)

        const dishes = await DbModels.dish.getWithImagesByCategory(categoryId)
        res.status(200).json(dishes)
    }
    static async update(req: Request, res: Response) {
        let updatedDish = req.body
        updatedDish = await DbModels.dish.update(updatedDish)
        res.status(200).json(updatedDish)
    }
    static async delete(req: Request, res: Response) {
        const id = Number(req.query.id)

        const deleted = await DbModels.dish.delete(id)
        res.status(200).json(deleted)
    }
}