import type { Dish, DishImage } from "@prisma/client";

export type DishCreationParams = Omit<Dish, 'id' | 'createdAt' | 'updatedAt'>
export type DishWithImages = Dish & { images: DishImage[] }