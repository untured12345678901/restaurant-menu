import type { Dish, DishCategory } from "@prisma/client";

export type DishCategoryCreationParams = Omit<DishCategory, 'id' | 'createdAt' | 'updatedAt'>
export type DishCategoryWithDishes = DishCategory & {dishes: Dish[]}