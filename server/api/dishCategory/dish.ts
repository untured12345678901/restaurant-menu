import { DbModels } from "@root/server/database/db";
import type { Request, Response } from "express";
import type { DishCategoryCreationParams } from "./types";
import type { DishCategory } from "@prisma/client";

export default class DishCategoryApiHandler {
    static async get(req: Request, res: Response) {
        const id = Number(req.query.id)
        const dishCategory = await DbModels.dishCategory.get(id)

        res.status(200).json(dishCategory)
    }
    static async getAll(req: Request, res: Response) {
        const dishCategories = await DbModels.dishCategory.getAll()

        res.status(200).json(dishCategories)
    }
    static async create(req: Request, res: Response) {
        const data = req.body as DishCategoryCreationParams
        const dishCategory = await DbModels.dishCategory.create(data)

        res.status(200).json(dishCategory)
    }
    static async update(req: Request, res: Response) {
        const updatedDishCategoryParams = req.body as DishCategory
        const updatedDishCategory = await DbModels.dishCategory.update(updatedDishCategoryParams)

        res.status(200).json(updatedDishCategory)
    }
    static async delete(req: Request, res: Response) {
        const id = Number(req.query.id)
        const deleted = await DbModels.dishCategory.delete(id)

        res.status(200).json(deleted)
    }
    
    static async getDishCategoriesWithDishes(req: Request, res: Response){
        const dishCategoriesWithDishes = await DbModels.dishCategory.getDishCategoriesWithDishes()

        res.status(200).json(dishCategoriesWithDishes)
    }
}