import type { Request, Response } from "express";
import type { UserLoginParams, UserRegistrationParams } from "./types";
import { validatePassword, validateUsername } from "@root/libs/validator";
import { DbModels, DbServices } from "@root/server/database/db";
import { Crypt } from "@root/libs/crypt";
import jwt from "jsonwebtoken"
import { AppConfig } from "@root/config";
import { UserRole } from "@root/server/database/types";

export default class AuthApiHandler {
    static async register(req: Request, res: Response) {
        const { username, password, role } = req.body as UserRegistrationParams
        const usernameValidationResult = validateUsername(username)
        const passwordValidationResult = validatePassword(password)
        const roleValid = Boolean(UserRole[role])

        if (!usernameValidationResult.valid || !passwordValidationResult.valid || !roleValid) {
            res.status(500).json("Неправильный пароль, имя пользователя или роль пользователя!")
            return
        }

        const passwordHash = Crypt.hash(password)
        const user = await DbModels.user.create({
            username,
            password: passwordHash,
            role
        })

        res.status(200).json(user)
    }
    static async login(req: Request, res: Response) {
        const { username, password } = req.body as UserLoginParams
        const canLogin = await DbServices.auth.tryLogin(username, password)

        if (!canLogin) {
            res.status(403).json("Неверный логин / пароль")
            return
        }
        const user = await DbModels.user.getByUsername(username)

        if (!user) {
            res.status(500).json("Не получается найти пользователя с таким именем в базе данных.")
            return
        }

        const accessToken = jwt.sign(
            {
                id: user.id,
                role: user.role
            },
            AppConfig.jwtSecret,
            { expiresIn: AppConfig.jwtExpiresIn }
        )
        res.cookie("access_token", accessToken, {
            secure: true,
            httpOnly: true
        })
        .status(200)
        .json(user)
    }
    static logout(res: Response) {
        return res
            .clearCookie("access_token")
            .status(200)
            .json("Выход совершён успешно.")
    }
}