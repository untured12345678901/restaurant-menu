import type { User} from "@prisma/client";
import { UserRole } from "@root/server/database/types";

// TODO USER_ROLE
export type UserRegistrationParams = Omit<User, 'id' | 'createdAt' | 'updatedAt' | 'role'> & { role: UserRole }
export type UserLoginParams = Pick<User, 'username' | 'password'>
export type UserCreationParams = Omit<User, 'id' | 'createdAt' | 'updatedAt'>