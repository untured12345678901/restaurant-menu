import express from "express"
import bodyParser from "body-parser";
import open from 'open'
import { DbModels, initDatabase } from "./database/db";
import { AppConfig } from "@root/config";
import AuthApiHandler from "./api/auth/auth";
import AuthApiPath from "@root/config/api/auth";
import cookieParser from "cookie-parser";
import FilesManager from "./utilitary/filesManager";
import DishApiPath from "@root/config/api/dish";
import AdminMiddleware from "./middleware/adminMiddleware";
import DishApiHandler from "./api/dish/dish";
import DishImageApiHandler from "./api/dishImage/dishImage";
import DishImageApiPath from "@root/config/api/dishImage";
import DishCategoryApiPath from "@root/config/api/dishCategory";
import DishCategoryApiHandler from "./api/dishCategory/dish";
import fileUpload from "express-fileupload";

async function runExpressServer() {
  const app = express()
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({extended: true}))
  app.use(express.static('dist'))
  app.use(cookieParser())
  app.use(fileUpload({
      createParentPath: true
  }));

  // Authorization
  app.post(AuthApiPath.login, AuthApiHandler.login)
  app.post(AuthApiPath.register, AuthApiHandler.register)
  app.post(AuthApiPath.logout, AuthApiHandler.logout)

  // Dish
  app.get(DishApiPath.get, DishApiHandler.get)
  app.get(DishApiPath.getWithImagesByCategory, DishApiHandler.getWithImagesByCategory)
  app.post(DishApiPath.create, AdminMiddleware, DishApiHandler.create)
  app.put(DishApiPath.update, AdminMiddleware, DishApiHandler.update)
  app.delete(DishApiPath.delete, AdminMiddleware, DishApiHandler.delete)

  // Dish images
  app.get(DishImageApiPath.get, DishImageApiHandler.getImageFile)
  app.post(DishImageApiPath.addImages, AdminMiddleware, DishImageApiHandler.addImages)
  app.delete(DishImageApiPath.deleteImage, AdminMiddleware, DishImageApiHandler.deleteImage)
  app.get(DishImageApiPath.getDishImages, DishImageApiHandler.getDishImages)

  // Dish category
  app.get(DishCategoryApiPath.get, DishCategoryApiHandler.get)
  app.get(DishCategoryApiPath.getAll, DishCategoryApiHandler.getAll)
  app.get(DishCategoryApiPath.getDishCategoriesWithDishes, DishCategoryApiHandler.getDishCategoriesWithDishes)
  app.post(DishCategoryApiPath.create, AdminMiddleware, DishCategoryApiHandler.create)
  app.put(DishCategoryApiPath.update, AdminMiddleware, DishCategoryApiHandler.update)
  app.delete(DishCategoryApiPath.delete, AdminMiddleware, DishCategoryApiHandler.delete)

  app.listen(AppConfig.expressPort, () => {
    console.log(`Server listening on port ${AppConfig.expressPort}`)
  });
}


function createFoldersIfNotExist() {
  const folders = [
    AppConfig.storageFolder,
    AppConfig.dishImagesFolder
  ]
  folders.map(FilesManager.createDirIfNotExist)
}

createFoldersIfNotExist();
initDatabase().then(runExpressServer).then(async () => {
  if (!AppConfig.developmentMode) {
    await open(`http://localhost:${AppConfig.expressPort}`)
  }
})