import { AppConfig } from "@root/config";
import type { DishImage } from "@prisma/client";
import type { UploadedFile } from "express-fileupload";
import FilesManager from "./filesManager";
import fs from "node:fs"
import path from "node:path";

export default class DishImageManager {
    static saveFile(dishImage: DishImage, file: UploadedFile) {
        const dir = this.resolveDishImageFolderPath(dishImage)
        if (!FilesManager.directoryIsEmpty(dir)) {
            this.deleteFile(dishImage)
        }

        FilesManager.createDirIfNotExist(dir)
        file.mv(this.resolveDishImageFilePath(dishImage))
    }
    static deleteFile(dishImage: DishImage) {
        const dir = this.resolveDishImageFolderPath(dishImage)
        for (const file of fs.readdirSync(dir)) {
            fs.unlinkSync(path.join(dir, file));
          }
        fs.rmdirSync(dir)
    }
    static resolveDishImageFolderPath(dishImage: DishImage) {
        return path.resolve(`${AppConfig.dishImagesFolder}/${dishImage.id}`)
    }
    static resolveDishImageFilePath(dishImage: DishImage) {
        return this.resolveDishImageFolderPath(dishImage) + '/' + dishImage.name
    }
}