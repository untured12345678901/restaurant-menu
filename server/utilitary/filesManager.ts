import fs from "node:fs"

export default class FilesManager {
    static createDirIfNotExist(path: string) {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path)
        }
    }
    static directoryIsEmpty(path: string) {
        return !fs.existsSync(path) || fs.readdirSync(path).length === 0
    }
}