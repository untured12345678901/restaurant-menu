import type { Request, Response } from "express";
import jwt from "jsonwebtoken"
import { AppConfig } from "@root/config";
import { UserRole } from "@root/server/database/types";

export default function AdminMiddleware(req: Request, res: Response, next: Function) {
    const token = req.cookies.access_token;
    if (!token) {
      return res.sendStatus(403);
    }
    try {
      const { role } = jwt.verify(token, AppConfig.jwtSecret) as {role: UserRole};
      if (role === UserRole.ADMIN) {
        return next();
      } else {
        return res.sendStatus(403);
      }
    } catch {
      return res.sendStatus(403);
    }
  };