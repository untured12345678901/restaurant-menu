import { PrismaClient } from "@prisma/client"
import { Logger } from "@server/logs/logger"
import UserModelDb from "./models/user/user"
import AuthServiceDb from "./services/auth"
import DishImageModelDb from "./models/dishImage/dishImage"
import DishModelDb from "./models/dish/dish"
import { extendWithDishImageMiddleware } from "./models/dishImage/middleware"
import DishCategoryModelDb from "./models/dishCategory/dishCategory"

export const DbModels = {
    user: null as any as UserModelDb,
    dish: null as any as DishModelDb,
    dishImage: null as any as DishImageModelDb,
    dishCategory: null as any as DishCategoryModelDb
}
export const DbServices = {
    auth: null as any as AuthServiceDb
}

export async function initDatabase() {
    let prisma = new PrismaClient({
        log: [
            {
                emit: 'event',
                level: 'error'
            },
            {
                emit: 'event',
                level: 'query'
            }
        ]
    })

    prisma.$on('query', (e) => {
        Logger.logQuery(e)
    })
    prisma.$on('error', e => {
        Logger.error(e)
    })

    prisma = await extendWithDishImageMiddleware(prisma)

    DbModels.user = new UserModelDb(prisma)
    DbModels.dish = new DishModelDb(prisma)
    DbModels.dishImage = new DishImageModelDb(prisma)
    DbModels.dishCategory = new DishCategoryModelDb(prisma)

    DbServices.auth = new AuthServiceDb(prisma)

    Object.freeze(DbModels)
    Object.freeze(DbServices)
}
  