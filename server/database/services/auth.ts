import type { PrismaClient } from "@prisma/client";
import type { IWithDatabase } from "../types";
import { Crypt } from "@root/libs/crypt";

export default class AuthServiceDb implements IWithDatabase {
    db: PrismaClient
    
    constructor(db: PrismaClient) {
        this.db = db
    }

    async tryLogin(username: string, password: string): Promise<boolean> {
        const user = await this.db.user.findFirst({
            where: {
                username
            }
        })
        if (!user) {
            return false
        }

        return Crypt.compare(password, user.password)
    }
}