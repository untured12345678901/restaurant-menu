import type { PrismaClient, User } from "@prisma/client";
import type { UserCreationParams } from "@root/server/api/auth/types";
import type { ICRUD, IWithDatabase } from "../../types";

export default class UserModelDb implements ICRUD<User>, IWithDatabase {
    db: PrismaClient
    
    constructor(db: PrismaClient) {
        this.db = db
    }
    
    async create(data: UserCreationParams): Promise<User> {
        const user = await this.db.user.create({
            data
        })
        return user
    }
    async get(id: number): Promise<User | null> {
        const user = await this.db.user.findFirst({
            where: {
                id
            }
        })

        return user
    }
    async getByUsername(username: string): Promise<User | null> {
        const user = await this.db.user.findFirst({
            where: {
                username
            }
        })

        return user
    }
    async update(updatedUser: User): Promise<User | null> {
        const user = await this.db.user.update({
            where: {
                id: updatedUser.id
            },
            data: updatedUser
        })

        return user
    }
    async delete(id: number): Promise<boolean> {
        const deletedUser = await this.db.user.delete({
            where: {
                id
            }
        })

        return Boolean(deletedUser)
    }
}