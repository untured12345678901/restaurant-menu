import type { DishCategory, PrismaClient } from "@prisma/client";
import type { ICRUD, IWithDatabase } from "../../types";
import type { DishCategoryCreationParams, DishCategoryWithDishes } from "@root/server/api/dishCategory/types";

export default class DishCategoryModelDb implements ICRUD<DishCategory>, IWithDatabase {
    db: PrismaClient

    constructor(db: PrismaClient) {
        this.db = db
    }

    async create(data: DishCategoryCreationParams) {
        const dishCategory = this.db.dishCategory.create({
            data
        })

        return dishCategory
    }
    async update(updatedDishCategory: DishCategory) {
        return await this.db.dishCategory.update({
            where: {
                id: updatedDishCategory.id
            },
            data: updatedDishCategory
        })
    }
    async get(id: number) {
        return await this.db.dishCategory.findFirst({
            where: {
                id
            }
        })
    }
    async getAll() {
        return await this.db.dishCategory.findMany()
    }
    async delete(id: number) {
        const deletedDishCategory = await this.db.dishCategory.delete({
            where: {
                id
            }
        })

        return Boolean(deletedDishCategory)
    }

    async getDishCategoriesWithDishes(): Promise<DishCategoryWithDishes[]> {
        const dishCategoriesWithDishes = await this.db.dishCategory.findMany({
            include: {
                dishes: true
            }
        })

        return dishCategoriesWithDishes
    }
}