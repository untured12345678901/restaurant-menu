import type { Dish, PrismaClient } from "@prisma/client";
import type { ICRUD, IWithDatabase } from "../../types";
import type { DishCreationParams } from "@root/server/api/dish/types";

export default class DishModelDb implements ICRUD<Dish>, IWithDatabase {
    db: PrismaClient

    constructor(db: PrismaClient) {
        this.db = db
    }

    async create(data: DishCreationParams) {
        return await this.db.dish.create({
            data
        })
    }
    async get(id: number) {
        return await this.db.dish.findFirst({
            where: {
                id
            }
        })
    }
    async getWithImagesByCategory(categoryId: number) {
        return await this.db.dish.findMany({
            where: {
                categoryId
            },
            include: {
                images: true
            }
        })
    }
    async update(updatedDish: Dish) {
        return await this.db.dish.update({
            where: {
                id: updatedDish.id
            },
            data: updatedDish
        })
    }
    async delete(id: number) {
        const deletedDish = this.db.dish.delete({
            where: {
                id
            }
        })

        return Boolean(deletedDish)
    }
}