import {PrismaClient, type DishImage} from "@prisma/client";
import DishImageManager from "@root/server/utilitary/dishImageManager";

export async function extendWithDishImageMiddleware(prisma: PrismaClient) {
  return await prisma.$extends({
    query: {
      dishImage: {
        async delete({args, query}) {
          const dishImage = await prisma.dishImage.findFirst({
            where: args.where
          }) as DishImage
          
          DishImageManager.deleteFile(dishImage)
          return await query(args)
        },
        async deleteMany({args, query}) {
            const dishImages = await prisma.dishImage.findMany({
                where: args.where
            })
  
            for (const dishImage of dishImages) {
                DishImageManager.deleteFile(dishImage)
            }
          
            return await query(args)
        }
      },
    }
  }) as any
}