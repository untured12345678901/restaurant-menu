import type { PrismaClient, DishImage } from "@prisma/client";
import type { IWithDatabase } from "../../types";
import type { UploadedFile } from "express-fileupload";
import DishImageManager from "@root/server/utilitary/dishImageManager";

export default class DishImageModelDb implements IWithDatabase {
    db: PrismaClient
    
    constructor(db: PrismaClient) {
        this.db = db
    }
    
    async create(data: Omit<DishImage, 'id'>, file: UploadedFile): Promise<DishImage> {
        const dishImage = await this.db.dishImage.create({
            data
        })
        DishImageManager.saveFile(dishImage, file)

        return dishImage
    }
    async get(id: number): Promise<DishImage | null> {
        return await this.db.dishImage.findFirst({
            where: {
                id
            }
        })
    }
    async getByDishId(dishId: number): Promise<DishImage[]> {
        return await this.db.dishImage.findMany({
            where: {
                dishId
            }
        })
    }
    async delete(id: number): Promise<boolean> {
        const deletedDishImage = await this.db.dishImage.delete({
            where: {
                id
            }
        })

        return Boolean(deletedDishImage)
    }
}