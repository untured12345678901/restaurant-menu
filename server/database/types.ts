import type { PrismaClient } from "@prisma/client"

export enum UserRole {
    ADMIN = "ADMIN",
    CUSTOMER = "CUSTOMER",
    GUEST = "GUEST"
}

export interface IWithDatabase {
    db: PrismaClient
}
export interface ICRUD<T> {
    create: (data: any) => Promise<T>
    get: (id: number) => Promise<T | null>
    update: (updatedModel: T) => Promise<T | null>
    delete: (id: number) => Promise<boolean>
}