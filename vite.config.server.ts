import { defineConfig } from 'vite';
import {fileURLToPath, URL} from "node:url";

export default defineConfig({
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '@root': fileURLToPath(new URL('./', import.meta.url)),
      '@server': fileURLToPath(new URL('./server', import.meta.url)),
    }
  },
  build: {
    minify: true,
    ssr: fileURLToPath(new URL('./server/main.ts', import.meta.url)),
    outDir: 'dist_server',
  },
  publicDir: false
});
