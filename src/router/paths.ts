export const RoutePaths = {
    MenuView: "/menu-view",
    MenuEdit: "/menu-edit",
    Login: "/login",
    Registration: "/registration"
}