import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import MenuViewPage from '@/pages/MenuViewPage.vue'
import MenuEditPage from '@/pages/MenuEditPage.vue'
import { RoutePaths } from './paths'
import RegistrationPage from '@/pages/RegistrationPage.vue'
import LoginPage from '@/pages/LoginPage.vue'

const DEFAULT_TITLE = "Ресторан"
const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: RoutePaths.MenuView,
      name: 'Меню',
      component: MenuViewPage
    },
    {
      path: RoutePaths.MenuEdit,
      name: 'Редактирование меню',
      component: MenuEditPage
      
    },
    {
      path: RoutePaths.Login,
      name: 'Вход',
      component: LoginPage
    },
    {
      path: RoutePaths.Registration,
      name: 'Регистрация',
      component: RegistrationPage
    },
    {
      path: "/",
      redirect: RoutePaths.MenuView
    }
  ]
})
router.beforeEach((to) => {
  document.title = to.name as string || DEFAULT_TITLE
})

export default router
