import { UserRole } from "@root/server/database/types";
import { RoutePaths } from "./paths"

const requirements = {
    [UserRole.ADMIN]: [
        RoutePaths.MenuEdit
    ]
}
const rolesWhitelistMap: Map<string, UserRole[]> = (() => {
    const map = new Map<string, UserRole[]>()
    
    for (const [role, paths] of Object.entries(requirements)) {
        for (const path of paths) {
            if (map.has(path)) {
                map.get(path)?.push(role as UserRole)
            } else {
                map.set(path, [role as UserRole])
            }
        }
    }
    
    return map
})()

export default class RouteRoleRequirement {
    static checkRoleHasRight(role: UserRole, path: string) {
        if (!rolesWhitelistMap.has(path)) {
            return true
        }
        if (rolesWhitelistMap.get(path)?.indexOf(role) !== -1) {
            return true
        }

        return false
    }
}