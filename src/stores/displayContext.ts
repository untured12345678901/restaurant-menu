import { defineStore } from 'pinia'

type Size = {
  width: number,
  height: number
}
type State = {
  screenSize: Size
}

const BREAKPOINTS = {
  MOBILE: 480,
  SMALL: 768
} as const

export const useDisplayContext = defineStore('displayContext', {
  state: () => ({
    screenSize: {
      width: window.innerWidth,
      height: window.innerHeight
    }
  } as State),
  getters: {
    isSmall: (state: State) => {
      return state.screenSize.width <= BREAKPOINTS.SMALL;
    },
    isMobile: (state: State) => {
      return state.screenSize.width <= BREAKPOINTS.MOBILE;
    },
    isVertical: (state: State) => {
      return state.screenSize.width < state.screenSize.height;
    },
    isHorizontal: (state: State) => {
      return state.screenSize.height < state.screenSize.width;
    }
  },
  actions: {
    updateScreenSizes() {
      this.screenSize = {
        width: window.innerWidth,
        height: window.innerHeight
      }
    }
  }
})

window.addEventListener('resize', () => {
  const context = useDisplayContext();
  context.updateScreenSizes()
})
