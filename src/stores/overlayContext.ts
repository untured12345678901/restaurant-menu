import { defineStore } from "pinia";

type State = {
    pendingRequests: number,
    lastChangeTime: number,
    overlayVisible: boolean
}

const MINIMAL_SHOW_TIME = 500
let timer = 0
function debounceVisibility() {
    const context = useOverlayContext()
    context.updateOverlayVisibility()
    clearTimeout(timer)
    setTimeout(() => context.updateOverlayVisibility(), MINIMAL_SHOW_TIME)
}

export const useOverlayContext = defineStore('overlayContext', {
    state: (() => ({
        pendingRequests: 0,
        overlayVisible: false,
        lastChangeTime: performance.now()
    } as State)),
    actions: {
        increment() {
            this.pendingRequests++
            if (!this.overlayVisible) {
                this.lastChangeTime = performance.now()
            }
        },
        decrement() {
            this.pendingRequests--
            debounceVisibility()
            if (this.pendingRequests < 0) {
                throw new Error("Something went wrong with pending requests number: Can't have negative number of pending requests")
            }
        },
        updateOverlayVisibility() {
            this.overlayVisible = (performance.now() - this.lastChangeTime < MINIMAL_SHOW_TIME) || (this.pendingRequests > 0)
        }
    }
})