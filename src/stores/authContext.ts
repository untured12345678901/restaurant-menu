import type { User } from "@prisma/client";
import { UserRole } from "@root/server/database/types";
import { defineStore } from "pinia";

export type UserPublicInfo = Omit<User, 'password'>
type State = {
    user: UserPublicInfo | null
}

export const useAuthContext = defineStore('authContext', {
    state: () => ({
        user: localStorage.getItem('authorized-user') ? JSON.parse(localStorage.getItem('authorized-user') as string) : null
    } as State),
    getters: {
        isAuthorized(state: State) {
            return Boolean(state.user)
        },
        isAdmin(state: State) {
            return state.user?.role === UserRole.ADMIN
        }
    },
    actions: {
        setLoggedUserInfo(user: UserPublicInfo) {
            if (!user) {
                return
            }
            this.user = user
            localStorage.setItem('authorized-user', JSON.stringify(user))
        },
        clearLoggedUserInfo() {
            this.user = null
            localStorage.removeItem('authorized-user')
        }
    }
})