import router from "@/router";
import { RoutePaths } from "@/router/paths";
import RouteRoleRequirement from "@/router/roleRequirement";
import { useAuthContext } from "@/stores/authContext";
import { UserRole } from "@root/server/database/types";
import { onUnmounted, watchEffect } from "vue";

export default function useWatchAllowedRoute() {
    const context = useAuthContext()
    const stopWatching = watchEffect(() => {
        const role = (context.user && context.user.role) || UserRole.GUEST
        const path = router.currentRoute.value.path
        
        if (!RouteRoleRequirement.checkRoleHasRight(role as UserRole, path)) {
            router.push(RoutePaths.MenuView)
        }
    })
    onUnmounted(stopWatching)
}