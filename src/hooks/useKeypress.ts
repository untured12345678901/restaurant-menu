import type { Key } from "ts-key-enum";
import { onUnmounted } from "vue";

export function useKeypress(key: Key, callback = () => {}) {
    function onKeyPress(e: KeyboardEvent) {
        if (e.key === key) {
            callback()
        }
    }
    
    window.addEventListener('keyup', onKeyPress)
    onUnmounted(() => window.removeEventListener('keyup', onKeyPress))
}