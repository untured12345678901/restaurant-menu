enum Palette {
    primaryLight = '#22d1f6',
    primaryMain = '#29c5e6',
    primaryDark = '#0e9ab8',
    secondaryLight = '#e73d19',
    secondaryMain = '#d63614',
    secondaryDark = '#b82c0e',
    tertiaryLight = '#6fda6f',
    tertiaryMain = '#55a455',
    tertiaryDark = '#376937',
    error = '#a00000',
    danger = '#a11212',
    warning = '#ff8800',
    success = '#61a650',
    light = '#ffffff',
    dark = '#000000',
    main = 'white',
    main1 = 'whitesmoke',
    main2 = 'darkgray',
    main3 = 'gray',
    mainText = 'black',
    mainTextLight = '#ebebeb'
}

let stylesElement: HTMLStyleElement | null = null
function mountPalette() {
    if (stylesElement) {
        return
    }
    
    stylesElement = document.createElement('style')
    stylesElement.innerHTML = `
body {
    --palette-primary-light: ${Palette.primaryLight};
    --palette-primary-main: ${Palette.primaryMain};
    --palette-primary-dark: ${Palette.primaryDark};
    --palette-secondary-light: ${Palette.secondaryLight};
    --palette-secondary-main: ${Palette.secondaryMain};
    --palette-secondary-dark: ${Palette.secondaryDark};
    --palette-tertiary-light: ${Palette.tertiaryLight};
    --palette-tertiary-main: ${Palette.tertiaryMain};
    --palette-tertiary-dark: ${Palette.tertiaryDark};
    
    --palette-error: ${Palette.error};
    --palette-danger: ${Palette.danger};
    --palette-warning: ${Palette.warning};
    --palette-success: ${Palette.success};
    --palette-light: ${Palette.light};
    --palette-dark: ${Palette.dark};
}
body {
    --palette-main-light: ${Palette.light};
    --palette-main: ${Palette.main};
    --palette-main-1: ${Palette.main1};
    --palette-main-2: ${Palette.main2};
    --palette-main-3: ${Palette.main3};
    --palette-main-text: ${Palette.mainText};
    --palette-main-text-light: ${Palette.mainTextLight};
}
`
    document.head.appendChild(stylesElement)
}
function unmountPalette() {
    if (!stylesElement) {
        return
    }
    document.head.removeChild(stylesElement)
    stylesElement = null
}

export {
    Palette,
    mountPalette,
    unmountPalette
}