import '@/styles/base/base.css'
import '@/styles/base/base-variables.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import axios from 'axios'
import { useOverlayContext } from './stores/overlayContext'

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')

const overlayContext = useOverlayContext()

axios.interceptors.request.use(config => {
    overlayContext.increment()
    
    return config
})
axios.interceptors.response.use(response => {
    overlayContext.decrement()

    return response
}, error => {
    overlayContext.decrement()

    throw error
})