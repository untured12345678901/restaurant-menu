import axios from "axios";
import { onError } from "../errorHandler";
import type { Dish } from "@root/libs/prismaTypes";
import DishApiPath from "@root/config/api/dish";
import type { DishCreationParams, DishWithImages } from "@root/server/api/dish/types";

export default class DishApi {
    static async get(id: number): Promise<Dish> {
        return axios.get(DishApiPath.get, {
            params: {
                id
            }
        })
            .then(result => result.data)
            .catch(onError)
    }
    static async getWithImagesByCategory(categoryId: number): Promise<DishWithImages[]> {
        return axios.get(DishApiPath.getWithImagesByCategory, {
            params: {
                categoryId
            }
        })
            .then(result => result.data)
            .catch(onError)
    }
    static async create(data: DishCreationParams): Promise<Dish> {
        return axios.post(DishApiPath.create, data)
            .then(result => result.data)
            .catch(onError)
    }
    static async update(updatedDish: Dish): Promise<Dish> {
        return axios.put(DishApiPath.update, updatedDish)
            .then(result => result.data)
            .catch(onError)
    }
    static async delete(id: number): Promise<boolean> {
        return axios.delete(DishApiPath.delete, {
            params: {
                id
            }
        })
            .then(result => result.data)
            .catch(onError)
    }
}