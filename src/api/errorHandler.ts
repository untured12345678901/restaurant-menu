import router from "@/router"
import { RoutePaths } from "@/router/paths"
import { useAuthContext } from "@/stores/authContext"
import type { AxiosError } from "axios"

export function onError(error: AxiosError) {
    if (error.status === 403) {
        useAuthContext().clearLoggedUserInfo()
    }

    console.log("ERROR:", error)
    throw error
}