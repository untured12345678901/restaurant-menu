import DishCategoryApiPath from "@root/config/api/dishCategory";
import axios from "axios";
import { onError } from "../errorHandler";
import type { DishCategoryCreationParams, DishCategoryWithDishes } from "@root/server/api/dishCategory/types";
import type { DishCategory } from "@root/libs/prismaTypes";

export default class DishCategoryApi {
    static async get(id: number): Promise<DishCategory> {
        return axios.get(DishCategoryApiPath.get, {
            params: {
                id
            }
        })
            .then(result => result.data)
            .catch(onError)
    }
    static async getAll(): Promise<DishCategory[]> {
        return axios.get(DishCategoryApiPath.getAll)
            .then(result => result.data)
            .catch(onError)
    }
    static async create(data: DishCategoryCreationParams): Promise<DishCategory> {
        return axios.post(DishCategoryApiPath.create, data)
            .then(result => result.data)
            .catch(onError)
    }
    static async update(updatedDishCategory: DishCategory): Promise<DishCategory> {
        return axios.put(DishCategoryApiPath.update, updatedDishCategory)
            .then(result => result.data)
            .catch(onError)
    }
    static async delete(id: number): Promise<boolean> {
        return axios.delete(DishCategoryApiPath.delete, {
            params: {
                id
            }
        })
            .then(result => result.data)
            .catch(onError)
    }
    
    static async getDishCategoriesWithDishes(): Promise<DishCategoryWithDishes[]> {
        return axios.get(DishCategoryApiPath.getDishCategoriesWithDishes)
            .then(result => result.data)
            .catch(onError)        
    }
}