import DishImageApiPath from "@root/config/api/dishImage";
import axios from "axios";
import { onError } from "../errorHandler";

export default class DishImageApi {
    static async getDishImages(dishId: number) {
        return await axios.get(DishImageApiPath.getDishImages, {
            params: {
                id: dishId
            }
        })
            .then(res => res.data)
            .catch(onError)
    }
    static async getImageFile(id: number) {
        return await axios.get(DishImageApiPath.get, {
            params: {
                id
            },
            responseType: 'blob'
        })
            .then(res => res.data)
            .catch(onError)
    }
    static async uploadDishImages(dishId: number, files: File[]) {
        const data = new FormData()
        
        data.set('dishId', dishId.toString())
        for (const file of files) {
            data.set(`file-${file.name}`, file)
        }

        return await axios.post(DishImageApiPath.addImages, data)
    }
    static async deleteDishImage(id: number) {
        return await axios.delete(DishImageApiPath.deleteImage, {
            params: {
                id
            }
        })
            .then(res => res.data)
            .catch(onError)
    }
}