import type { UserLoginParams, UserRegistrationParams } from "@root/server/api/auth/types";
import axios from "axios";
import { onError } from "../errorHandler";
import type { User } from "@prisma/client";
import AuthApiPath from "@root/config/api/auth";
import { useAuthContext } from "@/stores/authContext";

export default class AuthApi {
    static async register(data: UserRegistrationParams): Promise<User> {
        return axios.post(AuthApiPath.register, data)
            .then(result => result.data)
            .catch(onError)
    }
    static login(data: UserLoginParams) {
        return axios.post(AuthApiPath.login, data)
            .then(result => {
                console.log({result})
                useAuthContext().setLoggedUserInfo(result.data)
                return result.data
            })
            .catch(onError)
    }
    static logout() {
        return axios.post(AuthApiPath.logout)
            .then(() => useAuthContext().clearLoggedUserInfo())
            .catch(onError)
    }
}