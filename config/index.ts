const AppConfig = {
  get jwtSecret() {
    return process.env.JWT_SECRET as string
  },
  get jwtExpiresIn() {
    return process.env.JWT_EXPIRES_IN as string
  },
  get passwordSaltRound() {
    return process.env.PASSWORD_SALT_ROUNDS
  },
  get expressPort() {
    return process.env.EXPRESS_PORT
  },
  get storageFolder() {
    return process.env.STORAGE_FOLDER as string
  },
  get dishImagesFolder() {
    return process.env.DISH_IMAGES_FOLDER as string
  },
  get developmentMode() {
    return process.env.DEVELOPMENT_MODE === 'true'
  }
}

export {
  AppConfig
}