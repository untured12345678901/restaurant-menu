const ROOT = '/api/auth'
const AuthApiPath = {
    register: ROOT + '/register',
    login: ROOT + '/login',
    logout: ROOT + '/logout'
} as const

export default AuthApiPath