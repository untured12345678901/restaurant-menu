const ROOT = '/api/dish-image'
const DishImageApiPath = {
    addImages: ROOT + '/add',
    deleteImage: ROOT + '/delete',
    get: ROOT + '/get',
    getDishImages: ROOT + '/get-dish-images'
} as const

export default DishImageApiPath