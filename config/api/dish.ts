const ROOT = '/api/dish'
const DishApiPath = {
    create: ROOT + '/create',
    get: ROOT + '/get',
    getWithImagesByCategory: ROOT + '/get-with-images-by-category',
    update: ROOT + '/update',
    delete: ROOT + '/delete'
} as const

export default DishApiPath