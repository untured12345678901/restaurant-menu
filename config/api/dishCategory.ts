const ROOT = '/api/dish-category'
const DishCategoryApiPath = {
    get: ROOT + '/get',
    getAll: ROOT + '/get-all',
    create: ROOT + '/create',
    update: ROOT + '/update',
    delete: ROOT + '/delete',

    getDishCategoriesWithDishes: ROOT + '/get-dish-category-with-dishes'
}
export default DishCategoryApiPath