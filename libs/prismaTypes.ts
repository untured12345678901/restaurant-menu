import type {
    Dish as Prisma_Dish,
    DishImage as Prisma_DishImage,
    User as Prisma_User,
    DishCategory as Prisma_DishCategory
} from "@prisma/client"

export type Dish = Prisma_Dish
export type DishImage = Prisma_DishImage
export type User = Prisma_User
export type DishCategory = Prisma_DishCategory