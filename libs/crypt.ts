import {AppConfig} from "../config"
import passwoordHash from "password-hash"

const Crypt = {
  hash: (password: string) => passwoordHash.generate(password, {
    saltLength: Number(AppConfig.passwordSaltRound)
  }),
  compare: (password: string, hash: string) => passwoordHash.verify(password, hash)
};

export {
  Crypt
}