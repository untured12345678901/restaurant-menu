import PasswordValidator from "password-validator"

type ValidationResult = {
    valid: boolean,
    errorText: string
}

const schema = {
    username: new PasswordValidator()
        .is().min(4)
        .is().max(32)
        .has().not('@'),
    password: new PasswordValidator()
        .is().min(4)
        .is().max(32)
        .has().uppercase()
        .has().lowercase()
        .has().digits(1)
        .has().not().spaces()
}

function validateUsername(username: string): ValidationResult {
    const valid = Boolean(schema.username.validate(username))
    return {
        valid,
        errorText: valid ? '' : "Имя пользователя должно быть от 4 до 32 символов и не содержать символа \"@\""
    }
}
function validatePassword(password: string): ValidationResult {
    const valid = Boolean(schema.password.validate(password))
    return {
        valid,
        errorText: valid ? '' : "Пароль должен быть от 4 до 32 символов в длину, содержать символы верхнего" +
        "и нижнего регистра, содержать как минимум одну цифру и не иметь пробелов"
    }
}

export {
    validateUsername,
    validatePassword
}